﻿// ON-SCREEN MENU SCRIPT 
// BeeTLe BeTHLeHeM 2016 

const string SCRIPT_VERSION = "v0.6";
const bool NO_TIMER_MODE = true;

int SCREEN_ROWS;
int SCREEN_COLS;

const int MAX_LOGS = 100;

const string MAIN_MONITOR = "OSM_main";
IMyTextPanel mainMonitor;

int PB_LENGTH;
const string PB_FULL = "#"; // Progress Bar Full Glyph
const string PB_EMPTY = "."; // Progress Bar Empty Glyph

static Dictionary<string, string> subTypeNames; // Items Catalog
static List<string> log;

static List<string> grids;
string selectedGrid;

string currentState;
string lastCommand;
int tick;

int screenType;
string[] screenTypeDesc = new string[] { "Text", "LCD", "Wide" };

Dictionary<String, List<String>> currentScreen;
Dictionary<String, String> currentScreenMetadata;

List<Color> fontColors;
List<string> fontColorsDesc;
int selectedFontColor;

bool twinColumns;

public Program() {
    log = new List<String>();

    InitCurrentScreen();
    InitCurrentScreenMetadata();

    subTypeNames = LoadSubTypeDict();

    fontColors = new List<Color>(new Color[] { Color.Gray, Color.Blue, Color.Cyan, Color.Green,
        Color.Magenta, Color.Red, Color.White, Color.Yellow });
    fontColorsDesc = new List<string>(new string[] { "Gray", "Blue", "Cyan", "Green", "Magenta", "Red", "White", "Yellow" });
    selectedFontColor = 0;

    screenType = 0;
    SetScreenSize();

    twinColumns = false;

    currentState = "";
    lastCommand = "GO:MAINMENU";

    tick = 0;

    grids = ScanGrids();
    selectedGrid = null;

    bool storageLoading = Load();

    mainMonitor = GridTerminalSystem.GetBlockWithName(MAIN_MONITOR) as IMyTextPanel;

    if (NO_TIMER_MODE == true) {
        Runtime.UpdateFrequency = UpdateFrequency.Update10;
    }

    // Starting Screen

    PB_LENGTH = SCREEN_COLS - 2;

    if (mainMonitor != null) {
        mainMonitor.ContentType = ContentType.TEXT_AND_IMAGE;

        LoadScreen(lastCommand, !storageLoading);
        Render(MAIN_MONITOR);
    }
}

public bool Load() {
    bool load = false;

    if (Storage != null && Storage != "") {
        string[] stored = Storage.Split('|');

        if (stored.Length < 13) {
            Storage = "";
            AddToLog("Cleaning Storage String...");
            return false;
        }

        currentState = stored[0];
        lastCommand = stored[1];

        screenType = Int32.Parse(stored[2]);
        SetScreenSize();

        selectedFontColor = Int32.Parse(stored[3]);

        currentScreenMetadata["isList"] = stored[4];
        currentScreenMetadata["isMain"] = stored[5];
        currentScreenMetadata["totPages"] = stored[6];
        currentScreenMetadata["currentPage"] = stored[7];
        currentScreenMetadata["backState"] = stored[8];
        currentScreenMetadata["userChoice"] = stored[9];
        currentScreenMetadata["exeCallback"] = stored[10];
        currentScreenMetadata["command"] = stored[11];
        currentScreenMetadata["forceRefresh"] = stored[12];

        twinColumns = bool.Parse(stored[13]);

        load = true;
    }

    return load;
}

public void Save() {
    Storage = "" + currentState + "|" + lastCommand + "|" + screenType + "|" + selectedFontColor + "|" +
        currentScreenMetadata["isList"] + "|" +
        currentScreenMetadata["isMain"] + "|" +
        currentScreenMetadata["totPages"] + "|" +
        currentScreenMetadata["currentPage"] + "|" +
        currentScreenMetadata["backState"] + "|" +
        currentScreenMetadata["userChoice"] + "|" +
        currentScreenMetadata["exeCallback"] + "|" +
        currentScreenMetadata["command"] + "|" +
        currentScreenMetadata["forceRefresh"] + "|" +
        twinColumns;
}

public void Main(string argument, UpdateType updateSource) {
    tick++;
    Echo("Tick: " + tick);

    if (tick > 65535) { tick = 0; }

    if (mainMonitor == null)
    {
        Echo("ERROR: MAIN MONITOR '" + MAIN_MONITOR + "' NOT FOUND!");
        return;
    }

    List<String> cData = DecodeCommand(lastCommand);
    if (cData[0] == "EXEC" || currentScreenMetadata["forceRefresh"] == "1") {
        LoadScreen(lastCommand, false);
    } else if (cData[0] == "GO" && cData[1] != currentState) {
        LoadScreen(lastCommand, true);
    }

    Render(MAIN_MONITOR);

    Update(argument);
}

public void InitCurrentScreen() {
    currentScreen = new Dictionary<string, List<string>>();
    currentScreen.Add("header", new List<string>());
    currentScreen.Add("content", new List<string>());
}

public void InitCurrentScreenMetadata() {
    AddToLog(">> initCurrentScreenMetadata");

    currentScreenMetadata = new Dictionary<string, string>();
    currentScreenMetadata.Add("isList", "0");
    currentScreenMetadata.Add("isMain", "0");
    currentScreenMetadata.Add("totPages", "1");
    currentScreenMetadata.Add("currentPage", "0");
    currentScreenMetadata.Add("backState", "");
    currentScreenMetadata.Add("userChoice", "0");
    currentScreenMetadata.Add("exeCallback", "");
    currentScreenMetadata.Add("command", "");
    currentScreenMetadata.Add("forceRefresh", "0");
}

public void Update(string buttonAction) {
    int onScreenContentHeight = Int32.Parse(currentScreenMetadata["onScreenContentHeight"]);
    int userChoice = Int32.Parse(currentScreenMetadata["userChoice"]);
    int totPages = Int32.Parse(currentScreenMetadata["totPages"]);
    int currentPage = Int32.Parse(currentScreenMetadata["currentPage"]);

    AddToLog(">> Update : " + buttonAction);

    if (buttonAction != null && buttonAction != "") {
        switch (buttonAction) {
            case "C_UP": // Cursor up 
                if (currentScreenMetadata["isList"] == "1" && userChoice > 0) {
                    userChoice--;
                    currentScreenMetadata["userChoice"] = "" + userChoice;

                    // Autochange subpage 
                    if (totPages > 1 && userChoice < (currentPage * onScreenContentHeight)) {
                        currentPage--;
                        currentScreenMetadata["currentPage"] = "" + currentPage;
                    }
                }
                break;

            case "C_DOWN": // Cursor down 
                if (currentScreenMetadata["isList"] == "1" && userChoice < currentScreen["content"].Count - 1) {
                    userChoice++;
                    currentScreenMetadata["userChoice"] = "" + userChoice;

                    // Autochange subpage 
                    if (totPages > 1 && userChoice >= (currentPage + 1) * onScreenContentHeight) {
                        currentPage++;
                        currentScreenMetadata["currentPage"] = "" + currentPage;
                    }
                }
                break;

            case "SELECT": // Execute item or go back from a text page 
                string backState = currentScreenMetadata["backState"];
                if (currentScreenMetadata["isList"] == "0") {
                    // If there isn't a list on the page, return to the previous page 
                    if (backState == null || backState == "") {
                        backState = "MAINMENU";
                        currentScreenMetadata["backState"] = backState;
                    }

                    LoadScreen("GO:" + backState, true);
                } else {
                    // An item on a list has been selected to execute 
                    currentScreenMetadata["backState"] = currentState; // "Return" state 

                    // EXEC:<VALUE>=<PARAMS>
                    // GO:<VALUE>|<DISPLAY_LABEL>

                    string selectedItem = currentScreen["content"][userChoice];

                    currentScreenMetadata["exeCallBack"] = lastCommand;

                    int sep = selectedItem.IndexOf("|");
                    if (sep == -1)
                        lastCommand = selectedItem;
                    else
                        lastCommand = selectedItem.Substring(0, sep); // Cut the display label 

                    List<String> cData = DecodeCommand(lastCommand);

                    if (cData[0] == "EXEC")
                        LoadScreen(lastCommand, false);
                    else
                        LoadScreen(lastCommand, true);
                }
                break;

            case "SWITCH": // Change page on a multi-page 
                if (totPages > 1) {
                    currentPage++;
                    currentScreenMetadata["currentPage"] = "" + currentPage;
                }
                break;
        }
    }
}

public void LoadScreen(string command, bool initCSM)
{
    AddToLog(">> LoadScreen : " + command + ", " + initCSM);

    if (command == null)
        command = lastCommand;
    else
        lastCommand = command;

    List<String> cData = DecodeCommand(command);

    string cmdKey = cData[0];
    string cmdValue = cData[1];

    string[] parms = null;
    if (cData.Count == 3 && cData[2] != null && cData[2] != "")
    {
        parms = cData[2].Split(',');
    }

    InitCurrentScreen();

    if (initCSM == true)
    {
        Echo("Init initCurrentScreenMetadata");
        InitCurrentScreenMetadata();
    }

    if (cmdKey == "GO")
    {
        currentScreenMetadata["command"] = command;

        currentState = cmdValue;
        switch (currentState)
        {
            case "MAINMENU":
                Go_MainMenu();
                break;
            case "OXYINFO":
                Go_OxyInfo();
                break;
            case "ERGINFO":
                Go_EnergyInfo();
                break;
            case "REFLIST":
                Go_RefineryList();
                break;
            case "REFINV":
                Go_RefineryInventory(parms[0]);
                break;
            case "ASSLIST":
                Go_AssemblerList(1);
                break;
            case "ASSINV":
                Go_AssemblerInventory(parms[0]);
                break;
            case "CNTLIST":
                Go_ContainerList();
                break;
            case "CNTINV":
                Go_ContainerInventory(parms[0]);
                break;
            case "CONINV":
                Go_ConnectorInventory(parms[0]);
                break;
            case "GRIDMAN":
                Go_GridManagement();
                break;
            case "SETTINGS":
                Go_Settings();
                break;
            case "SHOWLOG":
                Go_ShowLog();
                break;
        }
    }
    else if (cmdKey == "EXEC")
    {
        if (cmdValue == "NOP")
        {
            LoadScreen("GO:" + parms[0], false);
        }
        else if (cmdValue == "CHANGEFONTCOLOR") {
            selectedFontColor++;
            if (selectedFontColor > fontColors.Count - 1)
            {
                selectedFontColor = 0;
            }
            LoadScreen("GO:SETTINGS", false);
        }
        else if (cmdValue == "TOGGLESCREENSIZE")
        {
            screenType++;
            if (screenType > 2) { screenType = 0; }
            SetScreenSize();
            LoadScreen("GO:SETTINGS", false);
        }
        else if (cmdValue == "RESCANGRIDS")
        {
            grids = ScanGrids();
            if (!grids.Contains(selectedGrid)) {
                selectedGrid = null;
            }
            LoadScreen("GO:GRIDMAN", false);
        }
        else if (cmdValue == "SELECTGRID")
        {
            if (parms[0] == "ALL")
                selectedGrid = null;
            else
                selectedGrid = parms[0];
            LoadScreen("GO:GRIDMAN", false);
        }
        else if (cmdValue == "TOGGLETWINCOLUMNS")
        {
            twinColumns = !twinColumns;
            LoadScreen("GO:SETTINGS", false);
        }
    }
}

public void Render(string monitorName) {
    List<String> pageRows = new List<String>();
    int userChoice = Int32.Parse(currentScreenMetadata["userChoice"]);

    int totPages = Int32.Parse(currentScreenMetadata["totPages"]);
    int currentPage = Int32.Parse(currentScreenMetadata["currentPage"]);

    int hRows = currentScreen["header"].Count;
    int cRows = currentScreen["content"].Count;

    if (hRows > 0) {
        pageRows.AddRange(currentScreen["header"]);
        pageRows.Add(" ");
    }

    AddToLog(">> Render");
    AddToLog("1 userChoice = " + userChoice);

    if (cRows > 0) {
        if (totPages == 1) {
            // Single-page screen 
            if (currentScreenMetadata["isList"] == "0") {
                pageRows.AddRange(currentScreen["content"]);
            } else {
                for (int c = 0; c < cRows; c++) {
                    String row = currentScreen["content"][c];
                    if (c == userChoice)
                        pageRows.Add("> " + row.Substring(row.IndexOf("|") + 1)); // Selected 
                    else
                        pageRows.Add("  " + row.Substring(row.IndexOf("|") + 1));
                }
            }
        } else {
            // Multi-page screen
            int onScreenContentHeight = Int32.Parse(currentScreenMetadata["onScreenContentHeight"]);

            int topItem = currentPage * onScreenContentHeight;
            int bottomItem = topItem + onScreenContentHeight;

            if (bottomItem > cRows - 1) {
                bottomItem = cRows;
            }

            if (currentPage > totPages - 1) {
                currentPage = 0;
                currentScreenMetadata["currentPage"] = "" + currentPage;
            }

            if (currentScreenMetadata["isList"] == "1") {
                userChoice = topItem;
                currentScreenMetadata["userChoice"] = "" + userChoice;
                AddToLog("2 userChoice = " + userChoice);
            }

            for (int r = topItem; r < bottomItem; r++) {
                if (currentScreenMetadata["isList"] == "0") {
                    pageRows.Add(currentScreen["content"][r]);
                } else {
                    if (r == userChoice)
                        pageRows.Add("> " + currentScreen["content"][r].Substring(currentScreen["content"][r].IndexOf("|") + 1)); // Selected 
                    else
                        pageRows.Add("  " + currentScreen["content"][r].Substring(currentScreen["content"][r].IndexOf("|") + 1));
                }
            }
        }

        while (pageRows.Count <= SCREEN_ROWS) {
            pageRows.Add(" ");
        }

        pageRows.Add("[" + (currentPage + 1) + " / " + totPages + "]");
    }

    AddToLog("3 userChoice = " + userChoice);

    PrintScreen(monitorName, pageRows);
}

public List<String> DecodeCommand(string fullCommand) {
    List<String> cData = new List<String>();

    // EXEC:<VALUE>=<PARAMS>
    // GO:<VALUE>|<DISPLAY_LABEL>
    // GO:<VALUE>=<PARAMS>|<DISPLAY_LABEL>

    string action = null;
    string key = null;
    string value = null;
    string parms = null;
    int sep = -1;

    sep = fullCommand.IndexOf("=");
    if (sep == -1) {
        action = fullCommand;
    } else {
        action = fullCommand.Substring(0, sep);
        parms = fullCommand.Substring(sep + 1);
    }

    sep = action.IndexOf(":");
    key = action.Substring(0, sep);
    value = action.Substring(sep + 1);

    cData.Add(key);
    cData.Add(value);
    cData.Add(parms);

    return cData;
}

public void PrintScreen(string monitorName, List<String> textRows) {
    IMyTextPanel tPanel = GridTerminalSystem.GetBlockWithName(monitorName) as IMyTextPanel;

    tPanel.Font = "Monospace";
    tPanel.FontSize = 0.5f;
    tPanel.FontColor = fontColors[selectedFontColor]; // Color.White;

    tPanel.WriteText(String.Join("\n", textRows.ToArray()), false);
}

// Screen States ////////////////////////////////////////////

public void Go_MainMenu() {
    List<String> header = new List<String>();
    List<String> content = new List<String>();

    header.Add("[ MAIN MENU ] :: OSM " + SCRIPT_VERSION);

    content.Add("GO:GRIDMAN|Grid Management");
    content.Add("GO:REFLIST|Refinery Inventory Info");
    content.Add("GO:ASSLIST|Assembler Inventory Info");
    content.Add("GO:CNTLIST|Container/Connector Inventory Info");
    content.Add("GO:OXYINFO|Oxygen Info Report");
    content.Add("GO:ERGINFO|Energy Info Report");
    content.Add("GO:SETTINGS|Edit Settings");

    currentScreen["header"] = header;
    currentScreen["content"] = content;

    SetScreenMetadata(header.Count, content.Count, "1", null);
}

public void Go_EnergyInfo() {
    List<String> header = new List<String>();
    List<String> content = new List<String>();

    string name = null;

    header.Add("[ ENERGY REPORT - " + (selectedGrid == null ? "ALL GRIDS" : selectedGrid) + "]");

    List<IMyTerminalBlock> reactors = new List<IMyTerminalBlock>();
    GridTerminalSystem.GetBlocksOfType<IMyReactor>(reactors);

    content.Add("// REACTORS");

    for (int e = 0; e < reactors.Count; e++) {
        IMyReactor reactor = (IMyReactor)reactors[e];

        if (selectedGrid == null || reactor.CubeGrid.CustomName == selectedGrid)
        {
            name = reactor.CustomName;

            float currentOutput = reactor.CurrentOutput;
            string currentOutputStr = currentOutput.ToString("0.00");

            List<MyInventoryItem> rContent = ReadBlockInventory(reactor.GetInventory(0));

            float uranium = 0.0f;
            for (int i = 0; i < rContent.Count; i++)
            {
                uranium += (float)rContent[i].Amount;
            }
            uranium = (float)Math.Round(uranium, 2);

            if (reactor.IsWorking == true)
            {
                content.Add(name + " >> " + currentOutputStr + " MW :: [" + uranium + " U]");
            }
            else
            {
                content.Add(name + " >> OFF :: [" + uranium + " U]");
            }
        }
    }
    content.Add(" ");

    List<IMyTerminalBlock> solarPanels = new List<IMyTerminalBlock>();
    GridTerminalSystem.GetBlocksOfType<IMySolarPanel>(solarPanels);

    content.Add("// SOLAR PANELS");

    for (int e = 0; e < solarPanels.Count; e++) {
        IMySolarPanel solarPanel = (IMySolarPanel)solarPanels[e];

        if (selectedGrid == null || solarPanel.CubeGrid.CustomName == selectedGrid)
        {
            name = solarPanel.CustomName;

            float currentOutput = solarPanel.CurrentOutput * 1000;
            string currentOutputStr = currentOutput.ToString("0.00");
            float maxOutput = solarPanel.MaxOutput * 1000;
            string maxOutputStr = maxOutput.ToString("0.00");

            if (solarPanel.IsWorking)
            {
                content.Add(name + " >> " + currentOutputStr + "/" + maxOutputStr + " kW");
            }
            else
            {
                content.Add(name + " >> OFF");
            }
        }
    }
    content.Add(" ");

    List<IMyTerminalBlock> batteries = new List<IMyTerminalBlock>();
    GridTerminalSystem.GetBlocksOfType<IMyBatteryBlock>(batteries);

    content.Add("// BATTERIES");

    for (int e = 0; e < batteries.Count; e++) {
        IMyBatteryBlock battery = (IMyBatteryBlock)batteries[e];

        if (selectedGrid == null || battery.CubeGrid.CustomName == selectedGrid)
        {
            name = battery.CustomName;

            float storedPower = battery.CurrentStoredPower;
            string storedPowerStr = storedPower.ToString("0.00");
            float maxPower = battery.MaxStoredPower;
            string maxPowerStr = maxPower.ToString("0.00");

            bool isCharging = battery.IsCharging;
            float currentInput = battery.CurrentInput;
            string currentInputStr = currentInput.ToString("0.00");
            float currentOutput = battery.CurrentOutput;
            string currentOutputStr = currentOutput.ToString("0.00");

            float time = 0;

            if (battery.IsWorking == true)
            {
                if (battery.IsCharging == true)
                {
                    if (currentInput > 0) { time = ((maxPower - storedPower) / currentInput) * 60; }
                    content.Add(name + ": " + storedPowerStr + "/" + maxPowerStr + " MW << " + currentInputStr + " MW :: " + time.ToString("0") + " min");
                    content.Add(DisplayCustomProgressBar(storedPower, maxPower, "+", PB_EMPTY));
                }
                else
                {
                    if (currentOutput > 0) { time = (storedPower / currentOutput) * 60; }
                    content.Add(name + ": " + storedPowerStr + "/" + maxPowerStr + " MW >> " + currentOutputStr + " MW :: " + time.ToString("0" + " min"));
                    content.Add(DisplayCustomProgressBar(storedPower, maxPower, "-", PB_EMPTY));
                }
            }
            else
            {
                content.Add(name + ": " + storedPowerStr + "/" + maxPowerStr + " MW :: OFF");
                content.Add(DisplayProgressBar(storedPower, maxPower));
            }
        }
    }

    currentScreen["header"] = header;
    currentScreen["content"] = content;

    SetScreenMetadata(header.Count, content.Count, "0", "MAINMENU");
    currentScreenMetadata["forceRefresh"] = "1";
}

public void Go_OxyInfo() {
    List<String> header = new List<String>();
    List<String> content = new List<String>();

    string name = null;

    header.Add("[ OXYGEN REPORT - " + (selectedGrid == null ? "ALL GRIDS" : selectedGrid) + "]");

    List<IMyTerminalBlock> oFarms = new List<IMyTerminalBlock>();
    GridTerminalSystem.GetBlocksOfType<IMyOxygenFarm>(oFarms);

    content.Add("// FARMS");

    for (int o = 0; o < oFarms.Count; o++) {
        IMyOxygenFarm oFarm = (IMyOxygenFarm)oFarms[o];

        if (selectedGrid == null || oFarm.CubeGrid.CustomName == selectedGrid)
        {
            name = oFarm.CustomName;

            Dictionary<String, String> oFarmDetail = ReadDetailedInfo(oFarm.DetailedInfo);
            if (oFarm.IsWorking == true)
                content.Add(name + ": " + oFarmDetail["Oxygen Output"]);
            else
                content.Add(name + ": OFF");
        }
    }
    content.Add(" ");

    List<IMyTerminalBlock> oTanks = new List<IMyTerminalBlock>();
    GridTerminalSystem.GetBlocksOfType<IMyGasTank>(oTanks);

    content.Add("// TANKS");

    for (int o = 0; o < oTanks.Count; o++) {
        IMyGasTank oTank = (IMyGasTank)oTanks[o];

        if (selectedGrid == null || oTank.CubeGrid.CustomName == selectedGrid)
        {
            name = oTank.CustomName;

            float filledRatioPercent = (float)oTank.FilledRatio * 100;
            string filledRatioPercentStr = filledRatioPercent.ToString("0.00");

            float capacity = oTank.Capacity;
            string capacityStr = capacity.ToString("0.00");

            bool stockpile = oTank.Stockpile;

            float filled = (float)oTank.FilledRatio * capacity;
            string filledStr = filled.ToString("0.00");

            if (oTank.IsWorking == true)
            {
                content.Add(name + ": " + filledStr + "/" + capacityStr + " L (" + filledRatioPercentStr + "%)");
                content.Add(DisplayProgressBar(filledRatioPercent, 100f));

            }
            else
            {
                content.Add(name + ": " + filledStr + "/" + capacityStr + " L (" + filledRatioPercentStr + "%) :: OFF");
                content.Add(DisplayProgressBar(filledRatioPercent, 100f));

            }
        }
    }
    content.Add(" ");

    List<IMyTerminalBlock> oGenerators = new List<IMyTerminalBlock>();
    GridTerminalSystem.GetBlocksOfType<IMyGasGenerator>(oGenerators);

    content.Add("// GENERATORS");

    for (int o = 0; o < oGenerators.Count; o++)
    {
        IMyGasGenerator oGenerator = (IMyGasGenerator)oGenerators[o];

        if (selectedGrid == null || oGenerator.CubeGrid.CustomName == selectedGrid)
        {
            name = oGenerator.CustomName;

            List<MyInventoryItem> gContent = ReadBlockInventory(oGenerator.GetInventory(0));

            float ice = 0.0f;
            for (int i = 0; i < gContent.Count; i++)
            {
                if (gContent[i].Type.SubtypeId.ToLower() == "ice")
                {
                    ice += (float)gContent[i].Amount;
                }
            }
            ice = (float)Math.Round(ice, 2);

            if (oGenerator.IsWorking == true)
                content.Add(name + " >> [" + ice + " ICE]");
            else
                content.Add(name + " >> OFF :: [" + ice + " ICE]");
        }
    }

    currentScreen["header"] = header;
    currentScreen["content"] = content;

    SetScreenMetadata(header.Count, content.Count, "0", "MAINMENU");
    currentScreenMetadata["forceRefresh"] = "1";
}

public void Go_GridManagement() {
    List<String> header = new List<String>();
    List<String> content = new List<String>();

    header.Add("[ GRID MANAGEMENT ]");

    content.Add("EXEC:NOP=GRIDMAN|Selected Grid: " + (selectedGrid == null ? "ALL GRIDS" : selectedGrid));
    content.Add("EXEC:RESCANGRIDS|Rescan Grids");
    content.Add("EXEC:SELECTGRID=ALL|Select: ALL GRIDS");

    for (int g = 0; g < grids.Count; g++) {
        content.Add("EXEC:SELECTGRID=" + grids[g] + "|Select: " + grids[g]);
    }
    content.Add("GO:MAINMENU|<< Back to Main Menu");

    currentScreen["header"] = header;
    currentScreen["content"] = content;

    SetScreenMetadata(header.Count, content.Count, "1", null);
}

public void Go_RefineryList() {
    List<String> header = new List<String>();
    List<String> content = new List<String>();

    header.Add("[ REFINERY LIST - " + (selectedGrid == null ? "ALL GRIDS" : selectedGrid) + "]");

    List<IMyTerminalBlock> refineries = new List<IMyTerminalBlock>();
    GridTerminalSystem.GetBlocksOfType<IMyRefinery>(refineries);

    for (int r = 0; r < refineries.Count; r++) {
        IMyRefinery rItem = (IMyRefinery)refineries[r];

        if (selectedGrid == null || rItem.CubeGrid.CustomName == selectedGrid)
        {
            string rName = rItem.CustomName;

            // Label is the name of the block, I need it to store it when selecting it 
            content.Add("GO:REFINV=" + rName + "|" + rName);
        }
    }

    // Return to the upper level screen (since this is a list) 
    content.Add("GO:MAINMENU|<< Back to Main Menu");

    currentScreen["header"] = header;
    currentScreen["content"] = content;

    SetScreenMetadata(header.Count, content.Count, "1", null);
}

public void Go_RefineryInventory(string name) {
    List<String> header = new List<String>();
    List<String> content = new List<String>();

    header.Add("[ REFINERY INFO: " + name + " ]");

    IMyRefinery refinery = GridTerminalSystem.GetBlockWithName(name) as IMyRefinery;

    content.AddRange(displayInventoryData("INPUT", refinery.GetInventory(0)));

    content.Add(" ");

    content.AddRange(displayInventoryData("OUTPUT", refinery.GetInventory(1)));

    currentScreen["header"] = header;
    currentScreen["content"] = content;

    SetScreenMetadata(header.Count, content.Count, "0", "REFLIST");
    currentScreenMetadata["forceRefresh"] = "1";
}

public void Go_AssemblerList(int mode) {
    List<String> header = new List<String>();
    List<String> content = new List<String>();

    List<IMyTerminalBlock> assemblers = new List<IMyTerminalBlock>();
    GridTerminalSystem.GetBlocksOfType<IMyAssembler>(assemblers);

    if (mode == 1) {
        // Show Assembler Inventory
        header.Add("[ ASSEMBLER LIST - " + (selectedGrid == null ? "ALL GRIDS" : selectedGrid) + "]");
    }

    for (int a = 0; a < assemblers.Count; a++) {
        IMyAssembler aItem = (IMyAssembler)assemblers[a];
        if (selectedGrid == null || aItem.CubeGrid.CustomName == selectedGrid)
        {
            string aName = aItem.CustomName;

            if (mode == 1)
            {
                // Label is the name of the block, I need it to store it when selecting it 
                content.Add("GO:ASSINV=" + aName + "|" + aName);
            }
        }
    }

    // Return to the upper level screen (since this is a list) 
    content.Add("GO:MAINMENU|<< Back to Main Menu");

    currentScreen["header"] = header;
    currentScreen["content"] = content;

    SetScreenMetadata(header.Count, content.Count, "1", null);
}

public void Go_AssemblerInventory(string name) {
    List<String> header = new List<String>();
    List<String> content = new List<String>();

    header.Add("[ ASSEMBLER INFO: " + name + " ]");

    IMyAssembler assembler = GridTerminalSystem.GetBlockWithName(name) as IMyAssembler;

    content.AddRange(displayInventoryData("INPUT", assembler.GetInventory(0)));

    content.Add(" ");

    content.AddRange(displayInventoryData("OUTPUT", assembler.GetInventory(1)));

    currentScreen["header"] = header;
    currentScreen["content"] = content;

    SetScreenMetadata(header.Count, content.Count, "0", "ASSLIST");
    currentScreenMetadata["forceRefresh"] = "1";
}

public void Go_ContainerList() {
    List<String> header = new List<String>();
    List<String> content = new List<String>();

    double[] vStats;

    header.Add("[ CONTAINER LIST - " + (selectedGrid == null ? "ALL GRIDS" : selectedGrid) + "]");

    List<IMyTerminalBlock> containers = new List<IMyTerminalBlock>();
    GridTerminalSystem.GetBlocksOfType<IMyCargoContainer>(containers);

    for (int c = 0; c < containers.Count; c++) {
        IMyCargoContainer cItem = (IMyCargoContainer)containers[c];

        if (selectedGrid == null || cItem.CubeGrid.CustomName == selectedGrid)
        {
            string cName = cItem.CustomName;
            vStats = ReadVolumeStats(cItem.GetInventory(0));

            // Label is the name of the block, I need it to store it when selecting it 
            content.Add("GO:CNTINV=" + cName + "|[Container] " + cName + " (" + vStats[2] + "%)");
        }
    }

    List<IMyTerminalBlock> connectors = new List<IMyTerminalBlock>();
    GridTerminalSystem.GetBlocksOfType<IMyShipConnector>(connectors);

    for (int c = 0; c < connectors.Count; c++) {
        IMyShipConnector cItem = (IMyShipConnector)connectors[c];

        if (selectedGrid == null || cItem.CubeGrid.CustomName == selectedGrid)
        {
            string cName = cItem.CustomName;
            vStats = ReadVolumeStats(cItem.GetInventory(0));

            // Label is the name of the block, I need it to store it when selecting it 
            content.Add("GO:CONINV=" + cName + "|[Connector] " + cName + " (" + vStats[2] + "%)");
        }
    }

    // Return to the upper level screen (since this is a list) 
    content.Add("GO:MAINMENU|<< Back to Main Menu");

    currentScreen["header"] = header;
    currentScreen["content"] = content;

    SetScreenMetadata(header.Count, content.Count, "1", null);
}

public void Go_ContainerInventory(string name) {
    List<String> header = new List<String>();
    List<String> content = new List<String>();

    header.Add("[ CONTAINER INFO: " + name + " ]");

    IMyCargoContainer container = GridTerminalSystem.GetBlockWithName(name) as IMyCargoContainer;

    content.AddRange(displayInventoryData(null, container.GetInventory(0)));

    currentScreen["header"] = header;
    currentScreen["content"] = content;

    SetScreenMetadata(header.Count, content.Count, "0", "CNTLIST");
    currentScreenMetadata["forceRefresh"] = "1";
}

public void Go_ConnectorInventory(string name) {
    List<String> header = new List<String>();
    List<String> content = new List<String>();

    header.Add("[ CONNECTOR INFO: " + name + " ]");

    IMyShipConnector connector = GridTerminalSystem.GetBlockWithName(name) as IMyShipConnector;

    content.AddRange(displayInventoryData(null, connector.GetInventory(0)));

    currentScreen["header"] = header;
    currentScreen["content"] = content;

    SetScreenMetadata(header.Count, content.Count, "0", "CNTLIST");
    currentScreenMetadata["forceRefresh"] = "1";
}

public void Go_Settings() {
    List<String> header = new List<String>();
    List<String> content = new List<String>();

    header.Add("[ SETTINGS ]");

    string monitorType = screenTypeDesc[screenType];
    string tcFlagStr = twinColumns == true ? "ON" : "OFF";

    content.Add("EXEC:CHANGEFONTCOLOR|Change Font Color (" + fontColorsDesc[selectedFontColor] + ")");
    content.Add("EXEC:TOGGLESCREENSIZE|Change Screen Size: " + monitorType);
    content.Add("GO:SHOWLOG|[Show Internal Log]");
    content.Add("EXEC:TOGGLETWINCOLUMNS|Toggle Twin Inventory Columns [" + tcFlagStr + "]");

    content.Add("GO:MAINMENU|<< Back to Main Menu");

    currentScreen["header"] = header;
    currentScreen["content"] = content;

    SetScreenMetadata(header.Count, 1, "1", null); // Force single page
}

public void SetScreenMetadata(int hCount, int cCount, string isList, string backState) {
    currentScreenMetadata["isList"] = isList;

    if (backState != null) {
        currentScreenMetadata["backState"] = backState;
    }

    DefinePagesForScreen(hCount, cCount);
}

public void Go_ShowLog() {
    List<String> header = new List<String>();
    List<String> content = new List<String>();

    header.Add("[ INTERNAL SCRIPT LOG ]");

    content.AddRange(log);

    currentScreen["header"] = header;
    currentScreen["content"] = content;

    SetScreenMetadata(header.Count, content.Count, "0", "SETTINGS");
}

// Utilities ////////////////////////////////////////////////

public List<string> displayInventoryData(string label, IMyInventory inventory) {
    List<string> invDisplay = new List<string>();

    string line = null;
    int column = 0;
    int magnLen = Magnitude(0).Length;

    double[] vStats = ReadVolumeStats(inventory);

    if (label == null)
        invDisplay.Add("// Volume: " + vStats[0] + "/" + vStats[1] + " (" + vStats[2] + "%)");
    else
        invDisplay.Add("// " + label + " - Volume: " + vStats[0] + "/" + vStats[1] + " (" + vStats[2] + "%)");

    invDisplay.Add(DisplayProgressBar((float)vStats[0], (float)vStats[1]));
    invDisplay.Add(" ");

    List<MyInventoryItem> invContent = ReadBlockInventory(inventory);

    if (invContent.Count == 0) {
        invDisplay.Add("-- EMPTY --");
    } else {
        int padSpaces = 0;

        for (int i = 0; i < invContent.Count; i++) {
            //float amount = (float)Math.Round((float)invContent[i].Amount, 2);
            int amount = (int) Math.Floor((decimal) invContent[i].Amount);

            string itemDesc = GetItemDescription(invContent[i].Type.SubtypeId, "" + invContent[i].Type.TypeId);

            if (twinColumns == false) {
                line = Magnitude(amount) + " " + amount.ToString().PadLeft(5) + " " + itemDesc;
                invDisplay.Add(line);
            } else {
                int descLen = (SCREEN_COLS / 2) - magnLen - 5 - 4;

                if (itemDesc.Length > descLen) { itemDesc = itemDesc.Substring(0, descLen); }

                if (column == 0) {
                    line = Magnitude(amount) + " " + amount.ToString().PadLeft(5) + " " + itemDesc;
                    padSpaces = (SCREEN_COLS / 2) - 2 - line.Length;
                    if (padSpaces < 0) { padSpaces = 0; }

                    column = 1;
                } else if (column == 1) {
                    if (padSpaces > 0) { for (int s = 0; s < padSpaces; s++) { line += " "; } }
                    line += " " + Magnitude(amount) + " " + amount.ToString().PadLeft(5) + " " + itemDesc;
                    invDisplay.Add(line);
                    column = 0;
                }
            }
        }
    }

    return invDisplay;
}

public void AddToLog(string text) {
    // Eliminate new lines inside the string 
    text = "::: " + text.Trim().Replace("\r\n", " ").Replace("\n", " ").Replace("\r", " ");

    log.AddRange(WordWrap(text));

    while (log.Count > MAX_LOGS) {
        log.RemoveAt(0);
    }
}

public void DefinePagesForScreen(int hRows, int cRows) {
    int onScreenContentHeight = SCREEN_ROWS - hRows - 1;
    currentScreenMetadata["onScreenContentHeight"] = "" + onScreenContentHeight;

    if (cRows > 0) {
        if (cRows < onScreenContentHeight) {
            currentScreenMetadata["totPages"] = "1";
        } else {
            int totPages = (int) Math.Ceiling((float) cRows/ onScreenContentHeight);
            currentScreenMetadata["totPages"] = "" + totPages;
        }
    }
}

public Dictionary<String, String> ReadDetailedInfo(string dInfo) { 
    string[] dInfoLines = dInfo.Split(new string[] { "\n" }, StringSplitOptions.None);

    Dictionary<String, String> dictDetails = new Dictionary<String, String>();

    for (int r = 0; r < dInfoLines.Length; r++) {
        int sep = dInfoLines[r].IndexOf(":");

        dictDetails.Add(dInfoLines[r].Substring(0, sep).Trim(), dInfoLines[r].Substring(sep + 1).Trim());
    }
    return dictDetails;
}

public List<MyInventoryItem> ReadBlockInventory(IMyInventory inventory) {
    List<MyInventoryItem> cContent = new List<MyInventoryItem>();
    inventory.GetItems(cContent, null);

    return cContent;
}

public string DisplayProgressBar(float amount, float max) {
    return DisplayCustomProgressBar(amount, max, PB_FULL, PB_EMPTY);
}

public string DisplayCustomProgressBar(float amount, float max, string pbFull, string pbEmpty) {
    int progress = (int)Percentage(amount, max, PB_LENGTH);

    if (progress > PB_LENGTH) {
        progress = PB_LENGTH;
    }

    if (progress < 0) {
        progress = 0;
    }

    string pb = "[";
    for (int p = 0; p < progress; p++) {
        pb += pbFull;
    }

    if ((PB_LENGTH - progress) > 0) {
        for (int p = 0; p < PB_LENGTH - progress; p++) {
            pb += pbEmpty;
        }
    }

    pb += "]";

    return pb;
}

public float Percentage(float current, float max, int scale) {
    float percent = (current / max) * scale;
    percent = (float)Math.Round(percent, 2);

    return percent;
}

public Dictionary<String, String> LoadSubTypeDict() {
    // Item definitions 

    Dictionary<String, String> subTypeNames = new Dictionary<String, String>();

    subTypeNames.Add("anglegrinderitem", "Grinder");
    subTypeNames.Add("automaticrifleitem", "Rifle");
    subTypeNames.Add("automaticrocketlauncher", "Rocket Launcher");
    subTypeNames.Add("bulletproofglass", "Bulletproof Glass");
    subTypeNames.Add("canvas", "Canvas");
    subTypeNames.Add("computer", "Computer");
    subTypeNames.Add("construction", "Construction Component");
    subTypeNames.Add("detector", "Detector Component");
    subTypeNames.Add("display", "Display");
    subTypeNames.Add("handdrillitem", "Hand Drill");
    subTypeNames.Add("interiorplate", "Interior Plate");
    subTypeNames.Add("gravitygenerator", "GravGen Component");
    subTypeNames.Add("girder", "Girder");
    subTypeNames.Add("largetube", "Large Steel Tube");
    subTypeNames.Add("medical", "Medical Component");
    subTypeNames.Add("medkit", "Medikit");
    subTypeNames.Add("metalgrid", "Metal Grid");
    subTypeNames.Add("motor", "Motor");
    subTypeNames.Add("nato_25x184mm", "NATO Ammo 25x184mm");
    subTypeNames.Add("nato_5P56x45mm", "NATO Ammo 5.56x45mm");
    subTypeNames.Add("powercell", "Power Cell");
    subTypeNames.Add("powerkit", "Power Kit");
    subTypeNames.Add("radiocommunication", "Radio Component");
    subTypeNames.Add("reactor", "Reactor Component");
    subTypeNames.Add("smalltube", "Small Steel Tube");
    subTypeNames.Add("solarcell", "Solar Cell");
    subTypeNames.Add("spacecredit", "Space Credits");
    subTypeNames.Add("steelplate", "Steel Plate");
    subTypeNames.Add("thrust", "Thruster Component");
    subTypeNames.Add("welderitem", "Welder");
    subTypeNames.Add("oxygenbottle", "Oxygen Bottle");

    subTypeNames.Add("cobalt_ore", "Cobalt Ore");
    subTypeNames.Add("gold_ore", "Gold Ore");
    subTypeNames.Add("ice_ore", "Ice");
    subTypeNames.Add("iron_ore", "Iron Ore");
    subTypeNames.Add("magnesium_ore", "Magnesium Ore");
    subTypeNames.Add("nickel_ore", "Nickel Ore");
    subTypeNames.Add("platinum_ore", "Platinum Ore");
    subTypeNames.Add("silicon_ore", "Silicon Ore");
    subTypeNames.Add("silver_ore", "Silver Ore");
    subTypeNames.Add("stone_ore", "Stone");
    subTypeNames.Add("gravel_ore", "Gravel");
    subTypeNames.Add("uranium_ore", "Uranium Ore");

    subTypeNames.Add("cobalt_ingot", "Cobalt Ingot");
    subTypeNames.Add("gold_ingot", "Gold Ingot");
    subTypeNames.Add("iron_ingot", "Iron Ingot");
    subTypeNames.Add("magnesium_ingot", "Magnesium Powder");
    subTypeNames.Add("nickel_ingot", "Nickel Ingot");
    subTypeNames.Add("platinum_ingot", "Platinum Ingot");
    subTypeNames.Add("silicon_ingot", "Silicon Wafer");
    subTypeNames.Add("silver_ingot", "Silver Ingot");
    subTypeNames.Add("stone_ingot", "Gravel");
    subTypeNames.Add("uranium_ingot", "Uranium Ingot");

    return subTypeNames;
}

public double[] ReadVolumeStats(IMyInventory inventory) {
    double[] vStats = new double[3];

    VRage.MyFixedPoint currentVolume = inventory.CurrentVolume;
    VRage.MyFixedPoint maxVolume = inventory.MaxVolume;

    double percentage = ((double)currentVolume / (double)maxVolume) * 100;

    vStats[0] = Math.Round((double)currentVolume, 2);
    vStats[1] = Math.Round((double)maxVolume, 2);
    vStats[2] = Math.Round((double)percentage, 2);

    return vStats;
}
public String GetItemDescription(string name, string type) {
    string itemDesc = "";
    string key = "";

    if (name == null) {
        name = "?";
    }

    if (type == null) {
        type = "?";
    }

    if (type.IndexOf("_Ore") != -1) {
        key = (name + "_ore").ToLower();
    } else if (type.IndexOf("_Ingot") != -1) {
        key = (name + "_ingot").ToLower();
    } else {
        key = name.ToLower();
    }

    if (subTypeNames.ContainsKey(key)) {
        itemDesc = subTypeNames[key];
    } else {
        itemDesc = key.ToUpper();
    }

    return itemDesc;
}

public string Magnitude(float value) {
    // Show an X for index of magnitude (1, 10, 100, 1000...) 

    string mStr = "";
    int mLevel = 0;

    if (value > 10000)
        mLevel = 5; // "HUGE"
    else if (value > 1000)
        mLevel = 4; // "HIGH"
    else if (value > 100)
        mLevel = 3; // "AVRG"
    else if (value > 10)
        mLevel = 2; // "LOW "
    else if (value > 1)
        mLevel = 1; // "NONE"

    mStr = "[";
    for (int m = 0; m < mLevel; m++) {
        mStr = mStr + "X";
    }
    for (int m = 0; m < 5 - mLevel; m++) {
        mStr = mStr + ".";
    }
    mStr = mStr + "]";

    return mStr;
}

public List<String> WordWrap(string text) {
    // http://stackoverflow.com/questions/10541124/wrap-text-to-the-next-line-when-it-exceeds-a-certain-length 

    string[] words = text.Split(' ');

    List<String> textRows = new List<String>();

    string line = "";
    foreach (string word in words) {
        if ((line + word).Length > SCREEN_COLS) {
            textRows.Add(line);
            line = "";
        }

        line += string.Format("{0} ", word);
    }

    if (line.Length > 0) {
        textRows.Add(line);
    }

    return textRows;
}

public void SetScreenSize() {
    //                    TEXT    LCD        WIDE
    // SCREEN_COLS        50        50        100
    // SCREEN_ROWS        32        18        32

    if (screenType == 0)
    {
        SCREEN_ROWS = 32;
        SCREEN_COLS = 50;
    }
    else if (screenType == 1)
    {
        SCREEN_ROWS = 18;
        SCREEN_COLS = 50;
    }
    else if (screenType == 2)
    {
        SCREEN_ROWS = 32;
        SCREEN_COLS = 100;
    }

    PB_LENGTH = SCREEN_COLS - 2;
}

public List<string> ScanGrids() {
    List<string> gridList = new List<string>();

    List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();
    GridTerminalSystem.GetBlocks(blocks);

    for (int b = 0; b < blocks.Count; b++) {
        IMyTerminalBlock bItem = (IMyTerminalBlock)blocks[b];
        string grid = bItem.CubeGrid.CustomName;
        if (!gridList.Contains(grid)) {
            gridList.Add(grid);
        }
    }

    return gridList;
}
