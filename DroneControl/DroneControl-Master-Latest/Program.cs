using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class DroneControlMasterTest : MyGridProgram
    {
        // Begin Here //

        // DroneControl-Master - Test Script

        const string MASTER_ANTENNA_NAME = "dc_antenna";
        // const string MASTER_PBLOCK_NAME = "dc_pb";

        const string SCRIPT_VERSION = "v0.1";
        const int MAX_LOG_MESSAGES = 16;
        const string MAIN_MONITOR = "DCM_main";

        int SCREEN_ROWS = 32;
        const int SCREEN_COLS = 51;

        const int LISTENER_INTERVAL = 10;

        IMyProgrammableBlock mPB;
        IMyRadioAntenna mAntenna;

        IMyTextPanel mainMonitor;

        Dictionary<string, string> droneMap; // DroneAddress, DroneName

        IMyUnicastListener uniListener;

        List<string> log;

        List<string> commandList;

        int tick;

        public DroneControlMasterTest()
        {
            tick = 0;

            log = new List<string>();
            commandList = new List<string>();

            droneMap = new Dictionary<string, string>(); // droneAddress, droneName
            uniListener = IGC.UnicastListener;
            
            mPB = GridTerminalSystem.GetBlockWithName(Me.CustomName) as IMyProgrammableBlock;
            mAntenna = GridTerminalSystem.GetBlockWithName(MASTER_ANTENNA_NAME) as IMyRadioAntenna;

            if (mAntenna != null) {
                mAntenna.AttachedProgrammableBlock = mPB.EntityId;
            }

            mainMonitor = GridTerminalSystem.GetBlockWithName(MAIN_MONITOR) as IMyTextPanel;

            Runtime.UpdateFrequency = UpdateFrequency.Update10;

            if (mainMonitor != null) {
                PrintLogOnScreen();
            }
        }

        public void Save() {
            //
        }

        public void Main(string argument, UpdateType updateSource)
        {
            tick++;

            Echo("DRONE CONTROL MASTER - SCRIPT TEST");
            Echo("Tick: " + tick);

            if (tick > 65535) { tick = 0; }

            if (mAntenna == null) {
                Echo("ERROR: Antenna '" + MASTER_ANTENNA_NAME + "' not found.");
                return;
            }

            if (mainMonitor == null)
            {
                Echo("ERROR: Monitor '" + MAIN_MONITOR + "' not found.");
                return;
            }

            //

            if (tick % LISTENER_INTERVAL == 0)
            {
                // Check messages
                if (uniListener.HasPendingMessage)
                {
                    MyIGCMessage uniMessage = uniListener.AcceptMessage();

                    string address = uniMessage.Source.ToString();
                    string tag = uniMessage.Tag;
                    string data = uniMessage.Data.ToString();

                    string[] mSplit = data.Split('|');

                    AddToLog("[" + tick + "] " + GetDroneName(address) + " << " + mSplit[0] + ":" + mSplit[1]);

                    if (mSplit[0] == "RESPONSE") {
                        if (mSplit[1] == "HELLO") {
                            Response_Hello(mSplit[2], mSplit[3]);
                        } else if (mSplit[1] == "POS") {
                            Response_Pos(address, mSplit[2]);
                        } else if (mSplit[1] == "LOG") {
                            data = mSplit[2];
                        }
                    }
                }
            }

            //

            if (commandList.Count > 0) {
                bool success;
                
                string command = commandList[0];
                string[] cSplit = command.Split(':');


                if (cSplit[0] == "ALL") {
                    success = BroadcastMessage(null, cSplit[1]);
                } else {
                    success = BroadcastMessage(cSplit[0], cSplit[1]);
                }

                commandList.RemoveAt(0);
            }

            //

            if (argument == "TEST1") {
                AddToLog("[" + tick + "] EXECUTING DIRECTIVE 'TEST1'");
                Command_Hello();
            } else if (argument == "TEST2") {
                AddToLog("[" + tick + "] EXECUTING DIRECTIVE 'TEST2'");
                Command_QueryPos(null);
            } else if (argument == "LOAD")
            {
                AddToLog("[" + tick + "] EXECUTING DIRECTIVE 'LOAD'");

                string customData = mPB.CustomData;
                string[] cSplit = customData.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                commandList.AddRange(cSplit);
            }

            PrintLogOnScreen();
        }

        public bool BroadcastMessage(string targetDrone, string message) {
            //message = tick + "|" + message;
            string[] mSplit = message.Split('|');

            if (targetDrone == null)
            {
                IGC.SendBroadcastMessage("ALL", message, TransmissionDistance.TransmissionDistanceMax);
                AddToLog("[" + tick + "] ALL >> " + mSplit[0] + ":" + mSplit[1]);
            } else
            {
                IGC.SendUnicastMessage(long.Parse(GetDroneAddress(targetDrone)), "SEND", message);
                AddToLog("[" + tick + "] " + targetDrone + " >> " + mSplit[0] + ":" + mSplit[1]);
            }

            return true;
        }

        public void Command_Hello() {
            Vector3D v3d = new Vector3D(mAntenna.Position);
            Vector3D maPos = Vector3D.Transform(v3d, mAntenna.WorldMatrix);

            string message = "ALL:INIT|HELLO|" + IGC.Me + "|" + maPos.X + ";" + maPos.Y + ";" + maPos.Z;
            commandList.Add(message);

            droneMap = new Dictionary<string, string>(); // droneAddress, droneName
        }

        public void Command_QueryPos(string droneTarget) {
            string message = "";

            message = (droneTarget == null ? "ALL" : droneTarget) + ":";
            message += "QUERY|POS";

            commandList.Add(message);
        }

        public void Command_Return(string droneTarget) {
            string message = "";

            if (droneTarget == null)
                message = "ALL:";
            else
                message = droneTarget + ":";
            message += "ACTION|RETURN";

            commandList.Add(message);
        }

        public void Response_Hello(string droneAddress, string droneName) {
            droneMap.Add(droneAddress, droneName);
            AddToLog("[" + tick + "] Mapping " + droneName + " address");
        }

        public void Response_Pos(string droneAddress, string coordStr) {
            string[] cSplit = coordStr.Split(';');
            double x = Math.Truncate(double.Parse(cSplit[0]) * 100) / 100;
            double y = Math.Truncate(double.Parse(cSplit[1]) * 100) / 100;
            double z = Math.Truncate(double.Parse(cSplit[2]) * 100) / 100;
            string message = "[" + tick + "] " + GetDroneName(droneAddress) + " position at (" + x + ", " + y + ", " + z + ")";
            AddToLog(message);
        }

        // Utilities //

        public void PrintLogOnScreen()
        {
            List<String> header = new List<String>();
            List<String> content = new List<String>();

            header.Add("[ LOG SCREEN ]");

            for (int l = 0; l < log.Count; l++)
            {
                content.Add(log[l]);
            }

            //

            List<String> pageRows = new List<String>();

            int hRows = header.Count;
            int cRows = content.Count;

            if (hRows > 0)
            {
                pageRows.AddRange(header);
                pageRows.Add(" ");
            }

            if (cRows > 0)
            {
                pageRows.AddRange(content);
            }

            while (pageRows.Count <= SCREEN_ROWS)
            {
                pageRows.Add(" ");
            }

            //

            IMyTextPanel tPanel = GridTerminalSystem.GetBlockWithName(MAIN_MONITOR) as IMyTextPanel;

            tPanel.Font = "Monospace";
            tPanel.FontSize = 0.5f;
            tPanel.FontColor = Color.Gray;

            tPanel.WriteText(String.Join("\n", pageRows), false);
        }

        public string GetDroneAddress(string droneName)
        {
            foreach (KeyValuePair<string, string> entry in droneMap)
            {
                if (droneName == entry.Value) { return entry.Key; }
            }

            return null;
        }

        public string GetDroneName(string droneAddress) 
        {
            foreach (KeyValuePair<string, string> entry in droneMap) {
                if (droneAddress == entry.Key) { return entry.Value; }
            }

            return null;
        }

        public void AddToLog(string text) {
            log.AddRange(WordWrap(text));

            while (log.Count > MAX_LOG_MESSAGES) {
                log.RemoveAt(0);
            }
        }

        public List<String> WordWrap(string text) {
            // http://stackoverflow.com/questions/10541124/wrap-text-to-the-next-line-when-it-exceeds-a-certain-length 

            string[] words = text.Split(' ');

            List<String> textRows = new List<String>();

            string line = "";
            foreach (string word in words) {
                if ((line + word).Length > SCREEN_COLS) {
                    textRows.Add(line);
                    line = "";
                }

                line += string.Format("{0} ", word);
            }

            if (line.Length > 0) {
                textRows.Add(line);
            }

            return textRows;
        }

        public Vector3D CoordsToVector3D(string coordStr)
        {
            string[] cSplit = coordStr.Split(';');
            
            double x = Double.Parse(cSplit[0]);
            double y = Double.Parse(cSplit[1]);
            double z = Double.Parse(cSplit[2]);
  
            Vector3D v3d = new Vector3D(x, y, z);      
            return v3d;
        }

        // End Here //
    }
}
