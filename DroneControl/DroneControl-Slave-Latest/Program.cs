﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class DroneControlSlave : MyGridProgram
    {
        // Begin Here //

        // DroneControl-Slave - Test Script

        const string SCRIPT_VERSION = "v0.1";

        const string ANTENNA_NAME = "dc_antenna";
        const string REMOTECONTROL_NAME = "dc_remote";

        const int LISTENER_INTERVAL = 10;

        IMyRemoteControl remCon;
        IMyRadioAntenna antenna;

        IMyUnicastListener droneListener;
        List<IMyBroadcastListener> listeners;

        List<string> commandList;

        long masterAddress;
        Vector3D masterAntennaPosition;

        int tick;

        public DroneControlSlave()
        {
            tick = 0;

            commandList = new List<string>();

            antenna = GridTerminalSystem.GetBlockWithName(ANTENNA_NAME) as IMyRadioAntenna;
            remCon = GridTerminalSystem.GetBlockWithName(REMOTECONTROL_NAME) as IMyRemoteControl;

            masterAddress = 0;

            droneListener = IGC.UnicastListener;

            IGC.RegisterBroadcastListener("ALL");

            listeners = new List<IMyBroadcastListener>();
            IGC.GetBroadcastListeners(listeners);
            
            Runtime.UpdateFrequency = UpdateFrequency.Update10;
        }

        public void Save() {
            //
        }

        public void Main(string argument, UpdateType updateSource)
        {
            tick++;

            Echo("DRONE CONTROL SLAVE - SCRIPT TEST");
            Echo("Tick: " + tick);

            if (tick > 65535) { tick = 0; }

            if (antenna == null) {
                Echo("ERROR: Antenna '" + ANTENNA_NAME + "' not found.");
                return;
            }
            if (remCon == null) {
                Echo("ERROR: Remote Control '" + REMOTECONTROL_NAME + "' not found.");
                return;
            }

            //

            if (tick % LISTENER_INTERVAL == 0)
            {
                // Check messages
                if (droneListener.HasPendingMessage)
                {
                    MyIGCMessage uniMessage = droneListener.AcceptMessage();

                    string address = uniMessage.Source.ToString();
                    string tag = uniMessage.Tag;
                    string data = uniMessage.Data.ToString();

                    ExecuteCommand(tag, data);
                }

                if (listeners[0].HasPendingMessage)
                {
	                MyIGCMessage message = new MyIGCMessage();
	                message = listeners[0].AcceptMessage();

	                string address = message.Source.ToString();
	                string tag  = message.Tag;
	                string data = message.Data.ToString();

                    ExecuteCommand(tag, data);
                }	
            }

            //

            if (commandList.Count > 0) {
                bool success;
                
                string command = commandList[0];
                string[] cSplit = command.Split(':');


                if (cSplit[0] == "ALL") {
                    success = BroadcastMessage(null, cSplit[1]);
                } else {
                    success = BroadcastMessage(cSplit[0], cSplit[1]);
                }

                commandList.RemoveAt(0);
            }

        }

        public void ExecuteCommand(string tag, string data)
        {
            if (tag == "ALL")
            {
                string[] mSplit = data.Split('|');
                if (mSplit[0] == "INIT")
                {
                    if (mSplit[1] == "HELLO")
                    {
                        Execute_Hello(mSplit[2], mSplit[3]);
                    }
                }
                else if (mSplit[0] == "QUERY")
                {
                    if (mSplit[1] == "POS")
                    {
                        Execute_QueryPos();
                    }
                }
                else if (mSplit[0] == "ACTION")
                {
                    if (mSplit[1] == "RETURN")
                    {
                        Execute_Return();
                    } else if (mSplit[1] == "GOTO")
                    {
                        Execute_Goto(mSplit[2], mSplit[3]);
                    }
                }
            }
        }

        public bool BroadcastMessage(string address, string message) {
            //message = tick + "|" + message;

            if (address == null)
                IGC.SendBroadcastMessage("ALL", message, TransmissionDistance.TransmissionDistanceMax);
            else
                IGC.SendUnicastMessage(long.Parse(address), "SEND", message);

            return true;
        }

        public void Execute_Hello(string mAddress, string maPos) {
            masterAddress = long.Parse(mAddress);
            masterAntennaPosition = CoordsToVector3D(maPos);

            string message = masterAddress + ":RESPONSE|HELLO|" + IGC.Me + "|" + remCon.CubeGrid.CustomName;
            commandList.Add(message);
        }

        public void Execute_QueryPos() {
            Vector3D v3d = Get_SelfPosition();

            string message = masterAddress + ":RESPONSE|POS|" + v3d.X + ";" + v3d.Y + ";" + v3d.Z;
            commandList.Add(message);
        }

        public void Execute_Goto(string targetCoords, string targetName)
        {
            Vector3D v3d = CoordsToVector3D(targetCoords);
            Goto(v3d, targetName);
            string message = masterAddress + ":RESPONSE|LOG|Going to '" + targetName + "' at (" + v3d.X + ", " + v3d.Y + ", " + v3d.Z + ")";
            commandList.Add(message);
        }

        public void Execute_Return() {
            Goto(masterAntennaPosition, "Master Antenna");
            string message = masterAddress + ":RESPONSE|LOG|Returning to Master Antenna";
            commandList.Add(message);
        }

        // Utilities //

        public Vector3D Get_SelfPosition() {
            Vector3D v3d = new Vector3D(remCon.Position);
            Vector3D worldPosition = Vector3D.Transform(v3d, remCon.WorldMatrix);
            return worldPosition;
        }

        public Vector3D CoordsToVector3D(string coordStr)
        {
            string[] cSplit = coordStr.Split(';');
            
            double x = Double.Parse(cSplit[0]);
            double y = Double.Parse(cSplit[1]);
            double z = Double.Parse(cSplit[2]);
  
            Vector3D v3d = new Vector3D(x, y, z);      
            return v3d;
        }

        public void Goto(Vector3D targetPos, string targetName) 
        {
            remCon.ClearWaypoints();
            remCon.AddWaypoint(targetPos, targetName);
            remCon.SetAutoPilotEnabled(true);
        }

        // End Here //
    }
}
